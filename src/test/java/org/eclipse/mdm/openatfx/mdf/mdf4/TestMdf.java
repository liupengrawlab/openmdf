package org.eclipse.mdm.openatfx.mdf.mdf4;

import de.rechner.openatfx.util.ODSHelper;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.asam.ods.AoSession;
import org.asam.ods.ApplicationStructure;
import org.asam.ods.InstanceElement;
import org.eclipse.mdm.openatfx.mdf.MDFConverter;
import org.junit.Test;
import org.omg.CORBA.ORB;

public class TestMdf {

  private String path =
      "C:\\01_Lokal\\03_Data\\MDF\\Beispieldaten\\Schallleistungsmessung_MOT__RSS__mf4.mf4";

  @Test
  public void test() throws Exception {
    ORB orb = ORB.init(new String[0], System.getProperties());
    Path path = Paths.get(this.path);
    MDFConverter reader = new MDFConverter();
    AoSession aoSession = reader.getAoSessionForMDF(orb, path);
    ApplicationStructure applicationStructure = aoSession.getApplicationStructure();

    InstanceElement ieTst =
        applicationStructure.getElementByName("tst").getInstances("*").nextOne();
    String id = ODSHelper.getStringVal(ieTst.getValue("mdf_file_id"));

    System.out.println(id);

  }

}

package org.eclipse.mdm.openatfx.mdf.mdf4;

/**
 * <p>
 * Title: {@link BlockReferenceTriple}
 * </p>
 * 
 * <b>Description:</b>
 * <p>
 * Storage class for a reference triple used at some points in an mdf file.
 * </p>
 *
 * @author Markus Renner
 *
 *         <p>
 *         Company: Peak Solution GmbH
 *         </p>
 *
 *         $Rev: $: Revision of last commit<br/>
 *         $Author: $: Author of last commit<br/>
 *         $Date: $: Date of last commit
 * 
 */
public class BlockReferenceTriple {
  private final long dgBlockOffset;
  private final long cgBlockOffset;
  private final long cnBlockOffset;

  public BlockReferenceTriple(long dgBlockOffset, long cgBlockOffset, long cnBlockOffset) {
    this.dgBlockOffset = dgBlockOffset;
    this.cgBlockOffset = cgBlockOffset;
    this.cnBlockOffset = cnBlockOffset;
  }

  /**
   * @return the dgBlockOffset
   */
  public long getDgBlockOffset() {
    return dgBlockOffset;
  }

  /**
   * @return the cgBlockOffset
   */
  public long getCgBlockOffset() {
    return cgBlockOffset;
  }

  /**
   * @return the cnBlockOffset
   */
  public long getCnBlockOffset() {
    return cnBlockOffset;
  }
}
